# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 11:17:03 2021
Based on 
@author: vatche.attarian

https://assets.tequipment.net/assets/1/26/MSO-DS1000Z_Programming_Guide.pdf
"""

import numpy
import matplotlib as mpl
from matplotlib import pyplot as plot
import sys
import time
import pyvisa as visa

#parameters that users can set
#number of times to acquire data
targetAcq=200
currAcq=1
yrange1=80
yrange2=150
#arbitrary threshold to set which works on 22 Jan rejecting touch in areas beside sensor
touchThreshold=120
touchThresholdVector=numpy.ones((targetAcq,1))+touchThreshold

noTouchingExperiment=0
notTouchingExperiment=0
sleepTime=0.5#time delay between acquisitions in addition to any 

false_touches=0
false_noTouches=0


#bookkeeping variable
rms_datas=[]
#look at connected resources to identify the DS1074 scope
rm = visa.ResourceManager()
# Get the USB device, e.g. 'USB0::0x1AB1::0x0588::DS1ED141904883'
instruments = rm.list_resources()
usb = list(filter(lambda x: 'USB' in x, instruments))
if len(usb) != 1:
    print('Bad instrument list', instruments)
    sys.exit(-1)

while currAcq<targetAcq:

    scope = rm.open_resource(usb[0], timeout=1000, chunk_size=2048000) # bigger timeout for long mem
    
    # Grab the raw data from channel 1
    scope.write(":STOP")
    
    # Get the timescale per division
    timescale = float(scope.query(":TIM:SCAL?"))
    
    # Get the timescale offset
    timeoffset = float(scope.query(":TIM:OFFS?")[0])
    voltscale = float(scope.query(':CHAN1:SCAL?')[0])
    
    # And the voltage offset
    #voltoffset = float(scope.query(":CHAN1:OFFS?")[0])
    
    scope.write(":WAV:POIN:MODE RAW")
    #rawdata = scope.query(":WAV:DATA? CHAN1").encode('ascii')[10:]
    
    scope.write(":WAV:DATA? CHAN1") #Request the data
    rawdata = scope.read_raw() #Read the block of data
    rawdata = rawdata[ 10 : ] #Drop the heading
    
    data_size = len(rawdata)
    sample_rate = float(str.strip(scope.query(':ACQ:SRAT?')))
    print('Data size:', data_size, "Sample rate:", sample_rate)
    
    scope.write(":KEY:FORCE")
    scope.write(":RUN")
    
    
    data = numpy.frombuffer(rawdata, 'B')
    
    # Walk through the data, and map it to actual voltages
    # This mapping is from Cibo Mahto
    # First invert the data
    data = data * -1 + 255
    
    # Now, we know from experimentation that the scope display range is actually
    # 30-229.  So shift by 130 - the voltage offset in counts, then scale to
    # get the actual voltage.
    data=-1*(data-(255+1)/2)
    data=data-numpy.mean(data[900:940])
    # Now, generate a time axis.
    times = numpy.linspace(timeoffset - 6 * timescale, timeoffset + 6 * timescale, num=len(data))
    samples=numpy.linspace(0,len(data)-1, len(data))
    # See if we should use a different time axis
    '''if (time[-1] < 1e-6):
        time = time * 1e9
        tUnit = "nS"   
    elif (time[-1] < 1e-3):
        time = time * 1e6
        tUnit = "uS"
    elif (time[-1] < 1):
        time = time * 1e3
        tUnit = "mS"
    else:
        tUnit = "S"'''
    
    
    # Figure to plot the data
    data_subset=data[2:-2]-numpy.mean(data[2:-2])
    
    #compute rms
    rms_data=numpy.mean(data_subset**2)
    
    rms_datas.append(rms_data)
    
    if currAcq==1:
        f1,(ax1,ax2)=plot.subplots(2)
        line1, = ax1.plot(data_subset)
        ax1.set_ylim(-50,50)
        line2, = ax2.plot(rms_datas)
        line3, = ax2.plot(touchThresholdVector)
        ax2.set_ylim(yrange1,yrange2)
        ax2.set_xlim(0,targetAcq)
        
        #define patches now that axes defined... may want to initialize figures above this area
        rect_noTouch = mpl.patches.Rectangle((1.05, 0.8), width=0.05, height=0.2, color="red", transform=ax2.transAxes,clip_on=False)
        rect_touch = mpl.patches.Rectangle((1.05, 0.8), width=0.05, height=0.2, color="green", transform=ax2.transAxes,clip_on=False)
    else:
        line2.set_data(range(len(rms_datas)), rms_datas)
        line1.set_data(range(len(data_subset)), data_subset)
        #stupid decision tree
        if numpy.round(rms_data)<touchThreshold:
            touch='yes'
            if notTouchingExperiment==1:
                false_touches+=1
            ax2.add_patch(rect_touch)
        else:
            touch='not'
            ax2.add_patch(rect_noTouch)
            
        ax1.title.set_text('rms='+str(numpy.round(rms_data))+'   ; '+touch+' touched')
        
    


    plot.pause(0.1)
    
    time.sleep(sleepTime)
    
    currAcq=currAcq+1

plot.ioff()    
scope.close()